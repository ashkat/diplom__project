const gulp = require('gulp');
const sass = require('gulp-sass');
const pug = require('gulp-pug');
const image = require('gulp-image');
const browserSync = require('browser-sync');

gulp.task('sass', function () {
    return gulp.src('./src/styles/**/*.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('./build/styles'));
  });

  gulp.task('pug', function buildHTML() {
    return gulp.src('src/pages/*.pug')
      .pipe(pug({
          pretty: true
      }))
      .pipe(gulp.dest("build/"));
    });


    gulp.task('image', function () {
        gulp.src('./src/img/*')
          .pipe(gulp.dest('./build/img'));
    });

      gulp.task('fonts', function () {
        gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./build/fonts'));
    });

    gulp.task('browserSync', function () {
        browserSync({
        server:{
            baseDir:'./build/'
        }, 
    })
    });

    gulp.task('watch', ['sass', 'pug', 'image', 'browserSync', 'fonts'], function () {	
        gulp.watch('./src/styles/*.scss', ['sass']);	
        gulp.watch('./src/pages/*.pug', ['pug']);	
        gulp.watch('./src/images/*', ['image']);	
        gulp.watch('build/*.html', browserSync.reload);	
        gulp.watch("./build/css/**/*.css").on("change", browserSync.reload);	
        gulp.watch('./build/js/**/*.js').on("change", browserSync.reload); });
        
        gulp.task('default', ['watch']);
        

